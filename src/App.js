import React from 'react';
import Formsy from 'formsy-react';
import { Input } from 'formsy-react-components';
import InputPassword from 'react-ux-password-field';
import PasswordStrengthMeter from 'react-password-strength-meter';
import PasswordWidget from './components/PasswordWidget';

const App = React.createClass({
  render() {
    const weaknessWords = ['Pessima', 'Scarsa', 'Buona', 'Forte', 'Ottima'];
    return (
      <div className="well">
        <Formsy.Form>
          <Input
            type="email"
            name="email"
            label="Email"
            validations="isEmail"
            validationErrors={{isEmail: 'This doesn’t look like a valid email address.'}}
            required
          />
          <PasswordWidget />
        </Formsy.Form>
      </div>
    );
  }
});

module.exports = App;

/*
Old input field:
  <Input
  type="password"
  name="pass"
  label="Pass"
  validations="minLength:4"
  validationErrors='Password troppo corta'
  required
  />

Links github projects:
 - InputPassword: https://github.com/seethroughtrees/react-ux-password-field
 - PasswordStrengthMeter: https://github.com/abhijeetnmishra/react-password-strength-meter

*/

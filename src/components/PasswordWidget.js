import React, { Component } from 'react'
import { Input } from 'formsy-react-components'
import MyInput from './MyInput';

export const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/


export default class PasswordWidget extends Component {

  renderRetypePasswordInput () {
    return (
      <Input
        type='text'
        label='Confirm Password'
        name='passwordConfirm'
        placeholder='Repeat password'
        validations='equalsField:password'
        validationErrors={{
          equalsField: 'Passwords do not match'
        }}
        required
      />
    )
  }

  render () {
    return (
      <div>
        <MyInput name="password"/>
        {this.renderRetypePasswordInput()}
      </div>
    )
  }

}

import React, { Component } from 'react'
import Formsy from 'formsy-react';

import { ComponentMixin, Row, Icon } from 'formsy-react-components';
import InputPassword from 'react-ux-password-field';

const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/

const MyInput = React.createClass({
  mixins: [Formsy.Mixin, ComponentMixin],
  changeValue(value, isValid, score) {
    console.log(value);
    this.setValue(value);
    //this.props.onChange(this.props.name, value);
  },
  renderElement(){
    const weaknessWords = ['Pessima', 'Scarsa', 'Buona', 'Forte', 'Ottima'];
    return (
      <InputPassword
        minScore={2}
        name="password"
        zxcvbn={true}
        strengthLang={weaknessWords}
        ref="element"
        id={this.getId()}
        onChange={this.changeValue}
        value={this.getValue()}
      />
    );
    /*
    {...this.props}
    <input
        className={className}
        label={null}
        disabled={this.isFormDisabled() || this.props.disabled}
    />
    */
  },
  render() {
    var element = this.renderElement();
    if (this.getLayout() === 'elementOnly') {
        return element;
    }
    var warningIcon = null;
    if (this.showErrors()) {
        warningIcon = (
            <Icon symbol="remove" className="form-control-feedback" />
        );
    }
    return (
        <Row
            {...this.getRowProperties()}
            htmlFor={this.getId()}
        >
            {element}
            {warningIcon}
            {this.renderHelp()}
            {this.renderErrorMessage()}
        </Row>
    );
  }
});

module.exports = MyInput;
